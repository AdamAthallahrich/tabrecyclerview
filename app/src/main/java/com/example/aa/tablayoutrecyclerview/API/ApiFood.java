package com.example.aa.tablayoutrecyclerview.API;

import com.example.aa.tablayoutrecyclerview.Model.Categoris;
import com.example.aa.tablayoutrecyclerview.Model.Meals;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiFood {
    @GET("latest.php")
    Call<Meals> getMeals();

    @GET("categories.php")
    Call<Categoris> getCategory();

    @GET("lookup.php")
    Call<Meals> getDetail(@Query("i") int i);
}
