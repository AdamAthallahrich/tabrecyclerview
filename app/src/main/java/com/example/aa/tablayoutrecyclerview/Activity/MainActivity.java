package com.example.aa.tablayoutrecyclerview.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.example.aa.tablayoutrecyclerview.Adapter.FavoriteAdapter;
import com.example.aa.tablayoutrecyclerview.Adapter.RecyclerAdapter;
import com.example.aa.tablayoutrecyclerview.Fragment.Favorite;
import com.example.aa.tablayoutrecyclerview.Fragment.Kiri;
import com.example.aa.tablayoutrecyclerview.Fragment.MapsActivity;
import com.example.aa.tablayoutrecyclerview.Model.FoodModel;
import com.example.aa.tablayoutrecyclerview.R;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements RecyclerAdapter.mClickListener, FavoriteAdapter.MyListener {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private int[] tabIcons = {
            R.drawable.ic_keyboard_arrow_left_white_24dp,
            R.drawable.ic_favorite,
            R.drawable.ic_keyboard_arrow_right_white_24dp
    };
    private String[] setTitle = {"Jenis Makanan", "Favorite", "Maps"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        viewPager = findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        /*ImageView user = findViewById(R.id.icon_user_profile);

        user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplication(), userProfile.class);
                startActivity(intent);
            }
        });*/

        tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                getSupportActionBar().setTitle(setTitle[tab.getPosition()]);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        setupTabIcons();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.save_data) {
            Intent userProfile = new Intent(MainActivity.this, userProfile.class);
            startActivity(userProfile);
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupTabIcons() {
        getSupportActionBar().setTitle(setTitle[0]);
        tabLayout.getTabAt(0).setIcon(tabIcons[0]);
        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
        tabLayout.getTabAt(2).setIcon(tabIcons[2]);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new Kiri(), "Kiri");
        adapter.addFrag(new Favorite(), "Favorite");
        adapter.addFrag(new MapsActivity(), "Maps");
        viewPager.setAdapter(adapter);
    }

    @Override
    public void mOnClickListener(int id) {
        Bundle bundle = new Bundle();
        bundle.putInt("id", id);
        Intent intent = new Intent(MainActivity.this, ShowDataDetail.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    public void mOnClickListenerFavorite(FoodModel model) {
        Bundle bundle = new Bundle();
        bundle.putInt("Id", model.getId());
        bundle.putInt("IdMakanan", model.getId_makanan());
        bundle.putString("StrGambarMakanan", model.getGambar_makanan());
        bundle.putString("StrNamaMakanan", model.getNama_makanan());
        bundle.putString("StrKategori", model.getKategori());
        bundle.putString("StrArea", model.getAsal_negara());
        bundle.putString("StrYoutube", model.getLink_youtube());
        bundle.putString("StrWeb", model.getLink_web());
        bundle.putString("StrInstruksi", model.getInstruksi_pembuatan());
        Intent i = new Intent(MainActivity.this, ShowDataDetail.class);
        i.putExtras(bundle);
        startActivity(i);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }
    }

}
