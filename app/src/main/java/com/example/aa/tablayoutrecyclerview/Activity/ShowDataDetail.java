package com.example.aa.tablayoutrecyclerview.Activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.aa.tablayoutrecyclerview.API.ApiClientFood;
import com.example.aa.tablayoutrecyclerview.API.ApiFood;
import com.example.aa.tablayoutrecyclerview.Helper.RealmHelper;
import com.example.aa.tablayoutrecyclerview.Model.FoodModel;
import com.example.aa.tablayoutrecyclerview.Model.Meals;
import com.example.aa.tablayoutrecyclerview.R;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import de.hdodenhof.circleimageview.CircleImageView;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShowDataDetail extends AppCompatActivity {

    String strMealThumb, strCategory, strInstructions, strMeal, strArea, strYoutube, strSources;
    int idMeals;
    Toolbar toolbar;
    ImageView loveFavorite, loveNonFavorite;
    ImageView youtube, web;
    RealmHelper realmHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_data_detail);

        toolbar = findViewById(R.id.toolbarDetail);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        loveFavorite = findViewById(R.id.loveFavorite);
        loveNonFavorite = findViewById(R.id.loveNonFavorite);

        final FoodModel favorite = new FoodModel();

        Realm.init(this);
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder().build();
        Realm realm = Realm.getInstance(realmConfiguration);
        realmHelper = new RealmHelper(realm);

        loveFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                favorite.setId_makanan(idMeals);
                favorite.setNama_makanan(strMeal);
                favorite.setGambar_makanan(strMealThumb);
                favorite.setAsal_negara(strArea);
                favorite.setInstruksi_pembuatan(strInstructions);
                favorite.setKategori(strCategory);
                favorite.setLink_web(strSources);
                favorite.setLink_youtube(strYoutube);
                realmHelper.save(favorite);
                loveFavorite.setVisibility(View.GONE);
                loveNonFavorite.setVisibility(View.VISIBLE);
                Toast.makeText(getApplicationContext(), "Sukses Menambahan ke Favorite", Toast.LENGTH_SHORT).show();
            }
        });

        loveNonFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                realmHelper.delete("id_makanan", idMeals);
                loveNonFavorite.setVisibility(View.GONE);
                loveFavorite.setVisibility(View.VISIBLE);
                Toast.makeText(getApplicationContext(), "Sukses Menghapus dari Favorite", Toast.LENGTH_SHORT).show();
            }
        });

        ApiFood themealdbService = ApiClientFood.getClient().create(ApiFood.class);
        Call<Meals> callCategories = themealdbService.getDetail(getIntent().getExtras().getInt("id"));
        callCategories.enqueue(new Callback<Meals>() {
            @Override
            public void onResponse(Call<Meals> call, Response<Meals> response) {
//                String strMealThumb,strCategory,strInstructions,strMeal;
//                int idMeals;
                strMealThumb = response.body().getMeals().get(0).getGambar();
                strCategory = response.body().getMeals().get(0).getStrCategory();
                strInstructions = response.body().getMeals().get(0).getStrInstructions();
                strMeal = response.body().getMeals().get(0).getNamaMakanan();
                strArea = response.body().getMeals().get(0).getArea();
                idMeals = response.body().getMeals().get(0).getIdMeal();
                strYoutube = response.body().getMeals().get(0).getYoutube();
                strSources = response.body().getMeals().get(0).getSource();
                setData();
            }

            @Override
            public void onFailure(Call<Meals> call, Throwable t) {

            }
        });
    }

    private void setData() {

        if (realmHelper.isNotAvailable(idMeals)) {
            loveNonFavorite.setVisibility(View.GONE);
            loveFavorite.setVisibility(View.VISIBLE);
        } else {
            loveFavorite.setVisibility(View.GONE);
            loveNonFavorite.setVisibility(View.VISIBLE);
        }

        toolbar.setTitle(strMeal);

        youtube = findViewById(R.id.ytb);
        web = findViewById(R.id.web);

        TextView textView = findViewById(R.id.instruction);
        TextInputEditText tvDetail = findViewById(R.id.namaMakananDetail);
        final TextInputEditText tvYoutube = findViewById(R.id.textYoutube);
        final TextInputEditText tvSources = findViewById(R.id.textWeb);
        TextInputEditText tvCategoriDetail = findViewById(R.id.categoriMakananDetail);
        tvCategoriDetail.setEnabled(false);
        tvDetail.setEnabled(false);
        RelativeLayout relativeLayout = findViewById(R.id.rlShow);
        if (tvDetail == null) {
            Toast.makeText(ShowDataDetail.this, "Pastikan Jaringan Internet Anda Lancar", Toast.LENGTH_LONG).show();
            relativeLayout.setVisibility(View.GONE);
        } else {
            relativeLayout.setVisibility(View.VISIBLE);
        }
        tvCategoriDetail.setText(strCategory);
        CircleImageView ivDetail = findViewById(R.id.imageDetail);
        tvDetail.setText(strArea);
        Picasso.get().load(strMealThumb).into(ivDetail);
        textView.setText(strInstructions);
        Picasso.get().load(strMealThumb).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                Drawable drawable = new BitmapDrawable(getBaseContext().getResources(), bitmap);
            }

            @Override
            public void onBitmapFailed(Exception e, Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        });
        tvSources.setText(strSources);
        tvYoutube.setText(strYoutube);
        tvSources.setEnabled(false);
        tvYoutube.setEnabled(false);
        web.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.setAction(Intent.ACTION_VIEW);
                i.addCategory(Intent.CATEGORY_BROWSABLE);
                i.setData(Uri.parse(String.valueOf(tvSources.getText())));
                startActivity(i);
            }
        });
        youtube.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.setAction(Intent.ACTION_VIEW);
                i.addCategory(Intent.CATEGORY_BROWSABLE);
                i.setData(Uri.parse(String.valueOf(tvYoutube.getText())));
                startActivity(i);
            }
        });
    }
}
