package com.example.aa.tablayoutrecyclerview.Activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.aa.tablayoutrecyclerview.Model.Preferences;
import com.example.aa.tablayoutrecyclerview.R;

public class userProfile extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        Toolbar mToolbar = findViewById(R.id.toolbarProfile);
        mToolbar.setTitle("User Preferences");
        mToolbar.setTitleTextColor(Color.WHITE);
        mToolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        final EditText nama, email, alamat;
        final Button save, edit;

        nama = findViewById(R.id.namaPengguna);
        email = findViewById(R.id.emailPengguna);
        alamat = findViewById(R.id.alamatPengguna);
        save = findViewById(R.id.save_data_profile);
        edit = findViewById(R.id.edit_data_profile);

        final String data = Preferences.getData(getApplicationContext());
        final String dataEmail = Preferences.getDataEmail(getApplicationContext());
        final String dataAlamat = Preferences.getDataAlamat(getApplicationContext());

        nama.setText(data);
        alamat.setText(dataAlamat);
        email.setText(dataEmail);
        if (!nama.getText().toString().equals("") || !alamat.getText().toString().equals("") || !email.getText().toString().equals("")) {
            nama.setEnabled(false);
            alamat.setEnabled(false);
            email.setEnabled(false);

            save.setVisibility(View.GONE);

            edit.setVisibility(View.VISIBLE);
        }

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Preferences.setData(getApplicationContext(), nama.getText().toString());
                Preferences.setDataEmail(getApplicationContext(), email.getText().toString());
                Preferences.setDataAlamat(getApplicationContext(), alamat.getText().toString());

                if (!nama.getText().toString().equals("") || !alamat.getText().toString().equals("") || !email.getText().toString().equals("")) {
                    nama.setText(data);
                    email.setText(dataEmail);
                    alamat.setText(dataAlamat);
                }

                nama.setEnabled(false);
                alamat.setEnabled(false);
                email.setEnabled(false);

                save.setVisibility(View.GONE);

                edit.setVisibility(View.VISIBLE);

                Intent i = new Intent(userProfile.this, userProfile.class);
                startActivity(i);
                finish();

            }
        });

        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                nama.setEnabled(true);
                alamat.setEnabled(true);
                email.setEnabled(true);

                save.setVisibility(View.VISIBLE);

                edit.setVisibility(View.GONE);

            }
        });

    }
}
