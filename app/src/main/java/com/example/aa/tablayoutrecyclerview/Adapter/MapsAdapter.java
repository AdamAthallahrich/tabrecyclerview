package com.example.aa.tablayoutrecyclerview.Adapter;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.aa.tablayoutrecyclerview.Model.MapsModel;
import com.example.aa.tablayoutrecyclerview.R;

import java.util.List;

public class MapsAdapter extends RecyclerView.Adapter<MapsAdapter.ViewHolder> {

    List<MapsModel> mapsModelArrayList;

    public MapsAdapter(List<MapsModel> mapsModelArrayList) {

        this.mapsModelArrayList = mapsModelArrayList;

    }

    @NonNull
    @Override
    public MapsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.maps_list, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MapsAdapter.ViewHolder viewHolder, int i) {

        viewHolder.img.setImageURI(Uri.parse(mapsModelArrayList.get(i).getGambar_lokasi()));
        viewHolder.txt.setText(mapsModelArrayList.get(i).getNama_lokasi());

    }

    @Override
    public int getItemCount() {
        return mapsModelArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView img;
        TextView txt;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            img = itemView.findViewById(R.id.imageMaps);
            txt = itemView.findViewById(R.id.textMaps);

        }
    }
}
