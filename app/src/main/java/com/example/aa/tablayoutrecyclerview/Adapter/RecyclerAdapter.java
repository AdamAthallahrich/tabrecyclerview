package com.example.aa.tablayoutrecyclerview.Adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.aa.tablayoutrecyclerview.Model.Meals;
import com.example.aa.tablayoutrecyclerview.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {
    ArrayList<Meals.meals> mListMeals;
    mClickListener mListener;

    public RecyclerAdapter(ArrayList<Meals.meals> mListMeals, mClickListener mListener) {
        this.mListMeals = mListMeals;
        this.mListener = mListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.kiri_list_row, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.setData(mListMeals.get(i).getNamaMakanan(), mListMeals.get(i).getGambar(), mListMeals.get(i).getStrCategory());
        final int idMeals = mListMeals.get(i).getIdMeal();
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.mOnClickListener(idMeals);
            }
        });
        /*viewHolder.textView.setText(mData.body().getMeals().get(i).getNamaMakanan());
        Picasso.get().load(mData.body().getMeals().get(i).getGambar()).into(viewHolder.imageView);*/
    }

    @Override
    public int getItemCount() {
        return mListMeals.size();
    }

    public interface mClickListener {
        void mOnClickListener(int id);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView textView, areaView;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageview);
            textView = itemView.findViewById(R.id.namaMakanan);
            areaView = itemView.findViewById(R.id.areaMakanan);
            CardView cardView = itemView.findViewById(R.id.cvShow);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(v.getContext(), "Klik : " + textView.getText(), Toast.LENGTH_SHORT).show();
                    /*itemView.getContext().startActivity(new Intent(itemView.getContext(),ShowDataDetail.class));*/
                }
            });
            if (textView == null) {
                Toast.makeText(itemView.getContext(), "Pastikan Jaringan Anda Lancar", Toast.LENGTH_SHORT).show();
                cardView.setVisibility(View.GONE);
            } else {
                cardView.setVisibility(View.VISIBLE);
            }
        }

        public void setData(String title, String img, String area) {
            textView.setText(title);
            areaView.setText(area);
            Picasso.get().load(img).into(imageView);
        }
    }

}
