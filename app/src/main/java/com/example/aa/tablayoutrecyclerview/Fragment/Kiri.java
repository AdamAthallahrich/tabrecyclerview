package com.example.aa.tablayoutrecyclerview.Fragment;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.aa.tablayoutrecyclerview.API.ApiClientFood;
import com.example.aa.tablayoutrecyclerview.API.ApiFood;
import com.example.aa.tablayoutrecyclerview.Adapter.CategoryRecyclerAdapter;
import com.example.aa.tablayoutrecyclerview.Adapter.MapsAdapter;
import com.example.aa.tablayoutrecyclerview.Adapter.RecyclerAdapter;
import com.example.aa.tablayoutrecyclerview.Helper.MapsHelper;
import com.example.aa.tablayoutrecyclerview.Model.Categoris;
import com.example.aa.tablayoutrecyclerview.Model.MapsModel;
import com.example.aa.tablayoutrecyclerview.Model.Meals;
import com.example.aa.tablayoutrecyclerview.R;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Kiri extends Fragment {

    MapsHelper realmHelper;
    RecyclerAdapter.mClickListener mListener;
    SwipeRefreshLayout mSwipeRefreshLayout;
    TextView txt;
    private RecyclerView recyclerView, recyclerViewHorizontal, rvMaps;

    public Kiri() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mListener = (RecyclerAdapter.mClickListener) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.kiri, container, false);
        recyclerViewHorizontal = view.findViewById(R.id.recycler_view_horizontal);

        rvMaps = view.findViewById(R.id.recycler_view_maps);

        txt = view.findViewById(R.id.text);

        mSwipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Refresh items
                refreshContent();
            }
        });

        ApiFood apiFoodCategory = ApiClientFood.getClient().create(ApiFood.class);
        Call<Categoris> foodCategoryCall = apiFoodCategory.getCategory();
        foodCategoryCall.enqueue(new Callback<Categoris>() {
            @Override
            public void onResponse(Call<Categoris> call, Response<Categoris> response) {
                Log.wtf("DataData", response.body().getCatogries().get(0).getStrCategory());
                recyclerViewHorizontal.setHasFixedSize(false);
                recyclerViewHorizontal.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
                recyclerViewHorizontal.setAdapter(new CategoryRecyclerAdapter(response));
            }

            @Override
            public void onFailure(Call<Categoris> call, Throwable t) {

            }
        });

        recyclerView = view.findViewById(R.id.recycler_view);
        ApiFood apiFood = ApiClientFood.getClient().create(ApiFood.class);
        Call<Meals> foodCall = apiFood.getMeals();
        foodCall.enqueue(new Callback<Meals>() {
            @Override
            public void onResponse(Call<Meals> call, Response<Meals> response) {
                Log.wtf("DataData", response.body().getMeals().get(0).getNamaMakanan());
                setRecyclerViewLatest(response.body().getMeals());
                /*recyclerView.setHasFixedSize(false);
                recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                recyclerView.setAdapter(new RecyclerAdapter(mListMeals, this.mListener));*/
            }

            @Override
            public void onFailure(Call<Meals> call, Throwable t) {

            }
        });

        Realm.init(getContext());
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder().build();
        Realm realm = Realm.getInstance(realmConfiguration);
        realmHelper = new MapsHelper(realm);

        setRvMaps();

        return view;
    }

    private void refreshContent() {

        Toast.makeText(getContext(), "Wait a second", Toast.LENGTH_SHORT).show();

        if (recyclerView.getAdapter() == null) {
            Toast.makeText(getContext(), "Pastikan Jaringan Anda Lancar", Toast.LENGTH_SHORT).show();
            txt.setVisibility(View.VISIBLE);
        } else {
            txt.setVisibility(View.GONE);
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // Stop animation (This will be after 3 seconds)
                mSwipeRefreshLayout.setRefreshing(false);
            }
        }, 3000);

        ApiFood apiFoodCategory = ApiClientFood.getClient().create(ApiFood.class);
        Call<Categoris> foodCategoryCall = apiFoodCategory.getCategory();
        foodCategoryCall.enqueue(new Callback<Categoris>() {
            @Override
            public void onResponse(Call<Categoris> call, Response<Categoris> response) {
                Log.wtf("DataData", response.body().getCatogries().get(0).getStrCategory());
                recyclerViewHorizontal.setHasFixedSize(false);
                recyclerViewHorizontal.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
                recyclerViewHorizontal.setAdapter(new CategoryRecyclerAdapter(response));
                if (recyclerViewHorizontal.getAdapter() == null) {
                    Toast.makeText(getContext(), "Pastikan Jaringan Anda Lancar", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Categoris> call, Throwable t) {

            }
        });

        ApiFood apiFood = ApiClientFood.getClient().create(ApiFood.class);
        Call<Meals> foodCall = apiFood.getMeals();
        foodCall.enqueue(new Callback<Meals>() {
            @Override
            public void onResponse(Call<Meals> call, Response<Meals> response) {
                Log.wtf("DataData", response.body().getMeals().get(0).getNamaMakanan());
                setRecyclerViewLatest(response.body().getMeals());
                /*recyclerView.setHasFixedSize(false);
                recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                recyclerView.setAdapter(new RecyclerAdapter(mListMeals, this.mListener));*/
            }

            @Override
            public void onFailure(Call<Meals> call, Throwable t) {

            }
        });

        setRvMaps();

    }

    public void setRecyclerViewLatest(ArrayList<Meals.meals> mListMeals) {
        recyclerView.setHasFixedSize(false);
        recyclerView.setBackgroundColor(Color.rgb(204, 204, 204));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        RecyclerAdapter recyclerFoodAdapter = new RecyclerAdapter(mListMeals, this.mListener);
        recyclerView.setAdapter(recyclerFoodAdapter);
    }

    public void setRvMaps() {
        List<MapsModel> mapsModelArrayList = realmHelper.getAllData();
        rvMaps.setHasFixedSize(false);
        rvMaps.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        rvMaps.setAdapter(new MapsAdapter(mapsModelArrayList));
    }

}
