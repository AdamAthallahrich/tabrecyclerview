package com.example.aa.tablayoutrecyclerview;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.aa.tablayoutrecyclerview.Helper.MapsHelper;
import com.example.aa.tablayoutrecyclerview.Model.InfoWindowData;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

import io.realm.Realm;
import io.realm.RealmConfiguration;

class InfoWindowCustom implements GoogleMap.InfoWindowAdapter {

    Context context;
    Realm realm;
    MapsHelper realmHelper;

    public InfoWindowCustom(Context context) {
        this.context = context;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(final Marker marker) {
        View v = ((Activity) context).getLayoutInflater()
                .inflate(R.layout.echo_info_window, null);

        TextView title = v.findViewById(R.id.info_window_title);
        ImageView imageView = v.findViewById(R.id.info_window_subtitle);

        title.setText(marker.getTitle());

        final InfoWindowData infoWindowData = (InfoWindowData) marker.getTag();

        int imageId = context.getResources().getIdentifier(infoWindowData.getImage().toLowerCase(),
                "drawable", context.getPackageName());
        imageView.setImageURI(Uri.parse(infoWindowData.getImage()));

        Button delete = v.findViewById(R.id.btnDeleteMarker);

        RealmConfiguration configuration = new RealmConfiguration.Builder().build();
        realm = Realm.getInstance(configuration);
        realmHelper = new MapsHelper(realm);

        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                realmHelper.delete(Integer.valueOf(marker.getId()));
            }
        });

        return v;
    }
}
