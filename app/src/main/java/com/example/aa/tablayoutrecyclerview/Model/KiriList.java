package com.example.aa.tablayoutrecyclerview.Model;

public class KiriList {
    private int image;
    private String name, position, age;

    public KiriList() {

    }

    public KiriList(int image, String name, String position, String age) {
        this.image = image;
        this.name = name;
        this.position = position;
        this.age = age;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }
}
