package com.example.aa.tablayoutrecyclerview.Model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class MapsModel extends RealmObject {

    @PrimaryKey
    private int id;
    private String nama_lokasi;
    private String gambar_lokasi;
    private Double latitude_lokasi;
    private Double longitude_lokasi;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama_lokasi() {
        return nama_lokasi;
    }

    public void setNama_lokasi(String nama_lokasi) {
        this.nama_lokasi = nama_lokasi;
    }

    public String getGambar_lokasi() {
        return gambar_lokasi;
    }

    public void setGambar_lokasi(String gambar_lokasi) {
        this.gambar_lokasi = gambar_lokasi;
    }

    public Double getLatitude_lokasi() {
        return latitude_lokasi;
    }

    public void setLatitude_lokasi(Double latitude_lokasi) {
        this.latitude_lokasi = latitude_lokasi;
    }

    public Double getLongitude_lokasi() {
        return longitude_lokasi;
    }

    public void setLongitude_lokasi(Double longitude_lokasi) {
        this.longitude_lokasi = longitude_lokasi;
    }
}