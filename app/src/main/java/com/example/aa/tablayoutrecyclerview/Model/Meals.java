package com.example.aa.tablayoutrecyclerview.Model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Meals {
    @SerializedName("meals")
    private ArrayList<meals> meal;

    public ArrayList<meals> getMeals() {
        return meal;
    }

    public class meals {
        @SerializedName("idMeal")
        private int idMeal;

        @SerializedName("strMealThumb")
        private String gambar;

        @SerializedName("strMeal")
        private String namaMakanan;

        @SerializedName("strCategory")
        private String strCategory;

        @SerializedName("strTags")
        private String strTags;

        @SerializedName("strInstructions")
        private String strInstructions;

        @SerializedName("strArea")
        private String area;

        @SerializedName("strSource")
        private String source;

        @SerializedName("strYoutube")
        private String youtube;

        public int getIdMeal() {
            return idMeal;
        }

        public String getGambar() {
            return gambar;
        }

        public String getNamaMakanan() {
            return namaMakanan;
        }

        public String getStrCategory() {
            return strCategory;
        }

        public String getStrTags() {
            return strTags;
        }

        public String getArea() {
            return area;
        }

        public String getStrInstructions() {
            return strInstructions;
        }

        public String getSource() {
            return source;
        }

        public String getYoutube() {
            return youtube;
        }
    }
}
