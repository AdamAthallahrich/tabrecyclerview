package com.example.aa.tablayoutrecyclerview.Model;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Preferences {
    static final String KEY_DATA = "Nama";
    static final String KEY_DATA1 = "Email";
    static final String KEY_DATA2 = "Alamat";

    /**
     * Pendlakarasian Shared Preferences yang berdasarkan paramater context
     */
    public static SharedPreferences getSharedPreference(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static void setData(Context context, String data) {
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(KEY_DATA, data);
        editor.apply();
    }

    public static String getData(Context context) {
        return getSharedPreference(context).getString(KEY_DATA, "");
    }

    public static void setDataEmail(Context context, String data) {
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(KEY_DATA1, data);
        editor.apply();
    }

    public static String getDataEmail(Context context) {
        return getSharedPreference(context).getString(KEY_DATA1, "");
    }

    public static void setDataAlamat(Context context, String data) {
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(KEY_DATA2, data);
        editor.apply();
    }

    public static String getDataAlamat(Context context) {
        return getSharedPreference(context).getString(KEY_DATA2, "");
    }
}
